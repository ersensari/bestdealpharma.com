export default [
  {
    name: "APP_LOGOUT",
    callback: function(e) {
      this.axios.post("/account/logout").then(response => {
        window.localStorage.removeItem("user");
        window.localStorage.removeItem("token");
        this.$store.dispatch("user/getAuthenticatedUser");

        this.$toastr("success", "Logout successfully");

        setTimeout(() => {
          window.location.href = "/";
        }, 500);
      });
    }
  },
  {
    name: "APP_PAGE_LOADED",
    callback: function(e) {}
  },
  {
    name: "APP_AUTH_FAILED",
    callback: function(e) {
      this.$router.push("/login");
      this.$message.error("Token has expired");
    }
  },
  {
    name: "APP_BAD_REQUEST",
    // @error api response data
    callback: function(msg) {
      this.$message.error(msg);
    }
  },
  {
    name: "APP_ACCESS_DENIED",
    // @error api response data
    callback: function(msg) {
      this.$message.error(msg);
      this.$router.push("/forbidden");
    }
  },
  {
    name: "APP_RESOURCE_DELETED",
    // @error api response data
    callback: function(msg) {
      this.$message.success(msg);
    }
  },
  {
    name: "APP_RESOURCE_UPDATED",
    // @error api response data
    callback: function(msg) {
      this.$message.success(msg);
    }
  },
  {
    name: "APP_SEARCH_DRUG",
    callback: function(searchText) {
      this.$router.push({
        name: "product-search",
        params: { pSearchText: searchText }
      });
      fbq("track", "productSearch", { searchText });
    }
  },
  {
    name: "APP_DETAIL_DRUG",
    callback: function(id, title) {
      this.$router.push({ name: "product", params: { id: id, title: title } });
      fbq("track", "productDetail", { product: title });
    }
  },
  {
    name: "APP_CLEAR_CART",
    callback: function() {
      this.$cookies.remove("shopping-cart");
      window.getApp.cartLength = 0;
      window.getApp.cart = [];
    }
  },
  {
    name: "APP_ADD_TO_CART",
    callback: function(value) {
      let cart = [];
      let cartCipherText = this.$cookies.get("shopping-cart");
      if (cartCipherText) {
        cart = this.$myUtil.decrypt(cartCipherText);
      }

      if (!cart.find(c => c.product.id === value.product.id)) {
        cart.push(value);
        this.$toastr(
          "success",
          value.product.title +
            " / " +
            value.product.strength +
            " has been added to cart"
        );
      } else {
        this.$toastr(
          "error",
          value.product.title +
            " / " +
            value.product.strength +
            " already added."
        );
      }
      this.$cookies.set("shopping-cart", this.$myUtil.encrypt(cart), "7d");
      window.getApp.cartLength = cart.length;
      window.getApp.cart = cart;
      fbq("track", "addToChart", { product: value.title });
    }
  }
];
