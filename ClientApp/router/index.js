import Vue from 'vue'
import VueRouter from 'vue-router'
import { routes } from './routes'
import NProgress from 'nprogress'

Vue.use(VueRouter)

let router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return { selector: to.hash }
    } else if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  NProgress.start()

  let __view = 'PageView'

  if (to.fullPath.indexOf('how-to-order') > -1) {
    __view = 'HowToOrder'
  }

  if (to.fullPath.indexOf('new-order') > -1) {
    __view = 'NewOrder'
  }

  if (to.fullPath.indexOf('product-search') > -1) {
    __view = 'ProductSearch'
  }

  if (to.fullPath.indexOf('product') > -1) {
    __view = 'ProductDetail'
  }

  if (to.fullPath.indexOf('account') > -1) {
    __view = 'Account'
  }

  if (to.fullPath.indexOf('faq') > -1) {
    __view = 'Faq'
  }

  if (to.fullPath.indexOf('policies') > -1) {
    __view = 'Policies'
  }

  if (to.hash === '#contact-us') {
    __view = 'ContactUs'
  }

  if (to.hash === '#about-us') {
    __view = 'AboutUs'
  }

  if (to.fullPath.indexOf('register') > -1) {
    __view = 'Register'
  }

  if (to.fullPath.indexOf('login') > -1) {
    __view = 'Login'
  }

  if (to.fullPath.indexOf('checkout') > -1) {
    __view = 'CheckOut'
  }

  if (to.fullPath.indexOf('shopping-cart') > -1) {
    __view = 'ShoppingCart'
  }
  window.__view = __view
  window.fbq('track', __view)
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!window.localStorage.getItem('token')) {
      next({
        path: '/login',
        params: { returnUrl: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    if (to.path === '/login' && window.localStorage.getItem('token')) {
      next({
        path: '/account'
      })
    } else if (to.path === '/please-confirm-email' && window.localStorage.getItem('token')) {
      next({
        path: '/account'
      })
    } else {
      next()
    }
  }
})

router.afterEach((to, from) => {
  NProgress.done()
})

export default router
