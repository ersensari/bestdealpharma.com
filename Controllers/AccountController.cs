using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using bestdealpharma.com.Data.Models;
using bestdealpharma.com.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Pages.Account.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;

namespace bestdealpharma.com.Controllers
{
  [Route("[controller]/[action]")]
  public class AccountController : Controller
  {
    private readonly Data.DbContext _context;


    private readonly SignInManager<IdentityUser> _signInManager;
    private readonly UserManager<IdentityUser> _userManager;

    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly IConfiguration _configuration;

    private readonly Providers.IAuthenticatedPersonProvider _authPerson;

    private readonly string[] _adminRoles = new string[] { "Admin", "Editor", "Call_Center" };

    private readonly IEmailService _emailService;


    public AccountController(
      UserManager<IdentityUser> userManager,
      SignInManager<IdentityUser> signInManager,
      RoleManager<IdentityRole> roleManager,
      IConfiguration configuration,
      Providers.IAuthenticatedPersonProvider authPerson,
      Data.DbContext context,
      IEmailService emailService
    )
    {
      _context = context;
      _userManager = userManager;
      _signInManager = signInManager;
      _roleManager = roleManager;
      _configuration = configuration;
      _authPerson = authPerson;
      _emailService = emailService;
    }


    [AllowAnonymous]
    [HttpPost]
    public async Task<IActionResult> Login([FromBody] UserModel login)
    {
      IActionResult response = Unauthorized();

      var result = await _signInManager.PasswordSignInAsync(login.Email, login.Password, false, false);

      if (result.IsNotAllowed) return BadRequest(new ErrorModel() { Message = "Your Email has not been confirmed!", Code = 5500 });

      if (!result.Succeeded) return response;

      var identifier = new IdentityUser
      {
        UserName = login.Email,
        Email = login.Email
      };


      var roles = await _userManager.GetRolesAsync(await _userManager.FindByEmailAsync(identifier.Email));
      if (login.forAdminPanel.GetValueOrDefault()
          && !roles.Any(x => _adminRoles.Contains(x)))
      {
        return Unauthorized();
      }

      var tokenString = GenerateJsonWebToken(identifier);
      response = Ok(new
      {
        token = tokenString,
        user = _authPerson.GetAuthenticatedUser(identifier.Email),
        returnUrl = login.forAdminPanel.GetValueOrDefault() ? "/admin" : "/"
      });

      return response;
    }

    [HttpPost]
    [Authorize]
    public async Task<IActionResult> Logout()
    {
      await _signInManager.SignOutAsync();
      return RedirectToAction("index", "home");
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> SendRescueCode([FromBody] UserModel model)
    {
      var user = await _userManager.FindByEmailAsync(model.Email);
      if (user != null)
      {
        var token = await _userManager.GeneratePasswordResetTokenAsync(user);

        await _emailService.SendEmail(user.Email, "bestdealpharma.com Password Recovery",
          $"<a href='{_configuration["Jwt:Issuer"]}/rescue-password?token={WebUtility.UrlEncode(token)}'>Create New Password</a>");


        return Ok(new
        {
          token = token,
          result = "success",
          message = "Your rescue code has been send your email address."
        });
      }
      else
      {
        return BadRequest(new
        {
          result = "error",
          message = "Oops! Entered email address could not be verified!"
        });
      }
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> CreateNewPassword([FromBody] RecoveryPasswordModel model)
    {
      var user = await _userManager.FindByEmailAsync(model.Email);
      if (user != null)
      {
        var result = await _userManager.ResetPasswordAsync(user, WebUtility.UrlDecode(model.Token), model.NewPassword);
        if (result.Succeeded)
        {
          await _emailService.SendEmail(user.Email, "bestdealpharma.com Change Password",
            "Your password has been changed successful");

          return Ok();
        }
        else
        {
          return BadRequest(result.Errors);
        }
      }
      else
      {
        return BadRequest();
      }
    }


    [HttpPost]
    [Authorize]
    public async Task<IActionResult> ChangePassword([FromBody] UserChangePasswordModel model)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }


      var jwt = new JwtSecurityTokenHandler().ReadJwtToken(model.Token);

      if (jwt.ValidTo <= DateTime.Now && jwt.Issuer == _configuration["Jwt:Issuer"])
      {
        var email = jwt.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Email)?.Value;
        var user = await _userManager.FindByEmailAsync(email);
        if (user != null)
        {
          var result = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
          if (result.Succeeded)
          {
            await _emailService.SendEmail(user.Email, "bestdealpharma.com Change Password",
              "Your password has been changed successful");

            return Ok();
          }
          else
          {
            return BadRequest(result.Errors);
          }
        }
        else
        {
          return BadRequest();
        }
      }
      else
      {
        return BadRequest();
      }
    }


    [AllowAnonymous]
    [HttpPost]
    public async Task<IActionResult> SendConfirmationEmail([FromBody] UserModel login)
    {
      IActionResult response = Unauthorized();

      var person = _context.People.Include(x => x.User).FirstOrDefault(x => x.User != null && x.User.Email == login.Email);
      if (person == null) return response;
      if (!person.User.EmailConfirmed)
      {
        var emailString = await CreateUserInfoEmailBodyAsync(person);
        await _emailService.SendEmail(person.User.Email, "Welcome to BestDealPharma.com, Don't forget to activate your account", emailString);

        return Ok();

      }

      return response;

    }

    [HttpPost]
    public async Task<IActionResult> Register([FromBody] RegisterDto model)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var identifier = new IdentityUser
      {
        UserName = model.Email,
        Email = model.Email,
        PhoneNumber = model.MobilePhone
      };
      var result = await _userManager.CreateAsync(identifier, model.Password);

      if (result.Succeeded)
      {
        await _userManager.AddToRoleAsync(identifier, "Guest");
        //await _signInManager.SignInAsync(identifier, false);
        var tokenString = GenerateJsonWebToken(identifier);

        var person = new Person()
        {
          Name = model.Name,
          Surname = model.Surname,
          Address = model.Address,
          State = model.State,
          City = model.City,
          Country = model.Country,
          BirthDate = model.BirthDate,
          HomePhone = model.HomePhone,
          MobilePhone = model.MobilePhone,
          ZipCode = model.ZipCode,
          UserId = identifier.Id,
          CreatedAt = DateTime.Now
        };

        _context.People.Add(person);

        var address = new Address()
        {
          State = model.State,
          City = model.City,
          Country = model.Country,
          MobilePhone = model.MobilePhone,
          ZipCode = model.ZipCode,
          AddressLine = model.Address,
          Person = person,
          AddressName = "default"
        };

        _context.Addresses.Add(address);

        _context.SaveChanges();

        var emailString = await CreateUserInfoEmailBodyAsync(person);
        await _emailService.SendEmail(person.User.Email, "Welcome to BestDealPharma.com, Don't forget to activate your account", emailString);

        await _emailService.SendEmail("info@bestdealpharma.com", "New customer has been registered.",
        "<html><body>" +
        "<center><a href='http://www.bestdealpharma.com'><img src='http://bestdealpharma.com/images/logo.png'/></a></center>" +
        "<h4>" + person.Name + "  " + person.Surname + " has been registered with " + model.Email + "</h4>" +
        "</body></html>");

        await _signInManager.SignOutAsync();

        var response = Ok(new
        {
          returnUrl = "/please-confirm-email"
        });
        return response;
      }
      else
      {
        return BadRequest(result.Errors);
      }
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> Activate(string UserId, string Token)
    {
      if (!string.IsNullOrWhiteSpace(UserId) && !string.IsNullOrWhiteSpace(Token))
      {

        var user = await _userManager.FindByIdAsync(UserId);
        if (user != null)
        {
          var result = await _userManager.ConfirmEmailAsync(user, Token);
          if (result.Succeeded)
          {
            return View();
          }
          else
          {
            return BadRequest(result.Errors);
          }
        }
        else
        {
          return BadRequest();
        }
      }
      else
      {
        return BadRequest("Activation informations are missing");
      }
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> ChangeEmailAddress(string UserId, string NewEmail, string Token)
    {
      if (!string.IsNullOrWhiteSpace(UserId) && !string.IsNullOrWhiteSpace(Token))
      {

        var user = await _userManager.FindByIdAsync(UserId);
        if (user != null)
        {
          var result = await _userManager.ChangeEmailAsync(user, NewEmail, Token);
          if (result.Succeeded)
          {
            var savedUser = _context.Users.FirstOrDefault(x => x.Id == user.Id);
            savedUser.UserName = NewEmail;
            savedUser.NormalizedUserName = NewEmail.ToUpperInvariant();
            _context.SaveChanges();
            return View();
          }
          else
          {
            return BadRequest(result.Errors);
          }
        }
        else
        {
          return BadRequest();
        }
      }
      else
      {
        return BadRequest("Account informations are missing");
      }
    }
    [HttpPost]
    public async Task<IActionResult> SendChangingEmailAddress(string oldEmail, string newEmail)
    {
      if (string.IsNullOrWhiteSpace(oldEmail) || string.IsNullOrWhiteSpace(newEmail))
      {
        return BadRequest("Please enter your email addresses");
      }
      if (oldEmail == newEmail)
      {
        return BadRequest("Please enter your new email address as different than current email address");
      }

      if (_context.Users.Any(x => x.Email == newEmail))
      {
        return BadRequest($"{newEmail} has been used by another member. Please check your new email address.");
      }

      var person = _context.People.Include(x => x.User).FirstOrDefault(x => x.User != null && x.User.Email == oldEmail);
      if (person == null)
      {
        return BadRequest("User cannot be found");
      }

      var emailString = await CreateUserInfoEmailBodyAsync(person, 1, newEmail);
      await _emailService.SendEmail(newEmail, "Change email address on BestDealPharma.com ", emailString);

      return Ok("Your email change link has been sent. Please check your inbox.");

    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> SendActivationMail(string email)
    {
      if (!string.IsNullOrWhiteSpace(email) && (new EmailAddressAttribute().IsValid(email)))
      {
        var person = _context.People.Include(x => x.User).FirstOrDefault(x => x.User != null && x.User.Email == email);

        if (person != null)
        {
          if (!person.User.EmailConfirmed)
          {
            var emailString = await CreateUserInfoEmailBodyAsync(person);
            await _emailService.SendEmail(person.User.Email, "Welcome to BestDealPharma.com, Don't forget to activate your account", emailString);
          }
          else
          {
            return BadRequest("User already activated");
          }
        }
        else
        {
          return BadRequest("User has not been found");
        }
        return Ok("Your activation code sent. Please check your inbox.");
      }
      else
      {
        return BadRequest("Email is not a valid format");
      }
    }


    ///
    /// type : 0 confirmation
    /// type : 1 change email
    ///
    private async Task<string> CreateUserInfoEmailBodyAsync(Person person, int type = 0, string newEmail = "")
    {

      string token = string.Empty;
      string activateUrl = string.Empty;

      switch (type)
      {

        case 0:
          token = await _userManager.GenerateEmailConfirmationTokenAsync(person.User);
          activateUrl = Url.Action("Activate", "Account", new
          {
            UserId = person.User.Id,
            Token = token
          }, protocol: HttpContext.Request.Scheme);

          break;
        case 1:
          token = await _userManager.GenerateChangeEmailTokenAsync(person.User, newEmail);
          activateUrl = Url.Action("ChangeEmailAddress", "Account", new
          {
            UserId = person.User.Id,
            newEmail = newEmail,
            Token = token
          }, protocol: HttpContext.Request.Scheme);
          break;
      }

      var htmlBody = new StringBuilder("<html>");
      htmlBody.Append("<head>");
      htmlBody.Append("<style>");
      htmlBody.Append("body{");
      htmlBody.Append("font-size:12px;");
      htmlBody.Append("font-color:#666;");
      htmlBody.Append("text-align:center;");
      htmlBody.Append("}");
      htmlBody.Append("</style>");
      htmlBody.Append("</head>");

      htmlBody.Append("<body>");
      htmlBody.Append("<div style='width:500px'>");
      htmlBody.Append("<center>");
      htmlBody.Append("<p><a href='http://www.bestdealpharma.com'><img src='http://bestdealpharma.com/images/logo.png'/></a></p>");

      htmlBody.Append($"<h3>{person.Name} {person.Surname}, Welcome to BestDealPharma.com</h3>");
      htmlBody.Append("<h4>Your one-stop online pharmacy</h4>");

      switch (type)
      {
        case 0:
          htmlBody.Append("<p>Please activate your account.</p>");
          htmlBody.Append($"<p><a href='{activateUrl}'>Click for activation</a></p>");

          break;
        case 1:
          htmlBody.Append($"<p><a href='{activateUrl}'>Click for change email address</a></p>");

          break;
      }

      htmlBody.Append(@"<br><br>
              <p>
                  <b>BestDealPharma</b> is a website created to send you the most reliable prescriptions at the cheapest
                  possible price from
                  countries like India, Turkey, England, Canada, South Africa, and New Zealand. Narcotics or other habit
                  forming
                  medications will not be sent.</p>
                <p>
                  <b>BestDealPharma</b> is not a licensed pharmacy; it is a prescription referral service. As a
                  prescription referral
                  service, we are able to refer you to our affiliated pharmacies and/or government approved dispensing
                  facilities to
                  ensure that you receive the highest quality product at the best possible price.
                </p>
                <p>
                  <b>BestDealPharma</b> has been created to network with countries such as India, Turkey, United
                  Kingdom,
                  Australia, and New
                  Zealand to ensure you receive your prescriptions at the best possible price and fastest possible time.
                </p>");

      htmlBody.Append("</center>");
      htmlBody.Append("</div>");
      htmlBody.Append("</body>");
      htmlBody.Append("</html>");

      return htmlBody.ToString();
    }

    private string CreateMD5(string input)
    {
      // Use input string to calculate MD5 hash
      using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
      {
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
        byte[] hashBytes = md5.ComputeHash(inputBytes);

        // Convert the byte array to hexadecimal string
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hashBytes.Length; i++)
        {
          sb.Append(hashBytes[i].ToString("X2"));
        }
        return sb.ToString();
      }

    }
    private string GenerateJsonWebToken(IdentityUser user)
    {
      var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
      var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

      var claims = new[]
      {
        new Claim(JwtRegisteredClaimNames.Email, user.Email),
        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
      };
      var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
        _configuration["Jwt:Issuer"],
        claims,
        expires: DateTime.Now.AddMonths(12),
        signingCredentials: credentials);

      return new JwtSecurityTokenHandler().WriteToken(token);
    }

    public class ErrorModel
    {
      public string Message { get; set; }

      public int Code { get; set; }
    }

    public class UserModel
    {
      [Required] public string Email { get; set; }

      [Required] public string Password { get; set; }

      public bool? forAdminPanel { get; set; }

      public bool isLockout { get; set; }
    }

    public class UserChangePasswordModel
    {
      [Required] public string CurrentPassword { get; set; }
      [Required] public string NewPassword { get; set; }
      [Required] public string ConfirmPassword { get; set; }
      [Required] public string Token { get; set; }
    }

    public class RecoveryPasswordModel
    {
      [Required] public string Email { get; set; }
      [Required] public string NewPassword { get; set; }
      [Required] public string ConfirmPassword { get; set; }
      [Required] public string Token { get; set; }
    }

    public class RegisterDto
    {
      [Required] public string Email { get; set; }

      [Required] public string Password { get; set; }

      [Required] public string Name { get; set; }
      [Required] public string Surname { get; set; }
      [Required] public string MobilePhone { get; set; }
      public string HomePhone { get; set; }
      [Required] public DateTime BirthDate { get; set; }
      [Required] public string Address { get; set; }
      [Required] public string City { get; set; }
      [Required] public string State { get; set; }
      [Required] public string ZipCode { get; set; }
      [Required] public string Country { get; set; }
    }
  }
}
