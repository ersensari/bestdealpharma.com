using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Text;

namespace bestdealpharma.com.Controllers
{
    public class HomeController : Controller
    {
        private readonly Data.DbContext _context;
        public HomeController(Data.DbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public FileResult SiteMap()
        {

            /*
            <?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            <url><loc>http://www.domain.com /</loc><changefreq>weekly</changefreq><priority>0.8</priority></url>
            <url>
            <loc>http://www.domain.com/catalog?item=vacation_hawaii</loc>
            <changefreq>weekly</changefreq>
            </url>
            <url>
            <loc>http://www.domain.com/catalog?item=vacation_new_zealand</loc>
            <lastmod>2008-12-23</lastmod>
            <changefreq>weekly</changefreq>
            </url>
            <url>
            <loc>http://www.domain.com/catalog?item=vacation_newfoundland</loc>
            <lastmod>2008-12-23T18:00:15+00:00</lastmod>
            <priority>0.3</priority>
            </url>
            <url>
            <loc>http://www.domain.com/catalog?item=vacation_usa</loc>
            <lastmod>2008-11-23</lastmod>
            </url>
            </urlset>
            */

            var products = _context.Products.Select(x => new { x.Id, x.Title }).ToList();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/how-to-order</loc><changefreq>weekly</changefreq><priority>0.8</priority></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/new-order</loc><changefreq>weekly</changefreq></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/order-refill</loc><changefreq>weekly</changefreq></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/faq</loc><changefreq>weekly</changefreq></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/policies</loc><changefreq>weekly</changefreq></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/#contact-us</loc><changefreq>weekly</changefreq></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/#about-us</loc><changefreq>weekly</changefreq></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/shopping-cart</loc><changefreq>weekly</changefreq></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/login</loc><changefreq>weekly</changefreq></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/register</loc><changefreq>weekly</changefreq></url>");
            sb.AppendLine("<url><loc>https://www.bestdealpharma.com/checkout</loc><changefreq>weekly</changefreq></url>");
            foreach (var item in products)
            {
                sb.Append($"<url><loc>https://www.bestdealpharma.com/product/{item.Id.ToString()}/{item.Title}</loc><changefreq>weekly</changefreq></url>");
            }
            sb.AppendLine("</urlset>");
            byte[] bytes = Encoding.UTF8.GetBytes(sb.ToString());

            return File(bytes, "application/xml", "bestdealpharma-sitemap.xml");
        }
    }
}
