using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace bestdealpharma.com.Controllers
{

  public class AdminController : Controller
  {
    private readonly Data.DbContext _context;
    public AdminController(Data.DbContext context)
    {
      _context = context;
    }
    public IActionResult Index()
    {
      return View();
    }

    public IActionResult Error()
    {
      return View();
    }
  }
}
