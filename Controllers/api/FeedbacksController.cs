using bestdealpharma.com.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace bestdealpharma.com.Controllers.api
{
  [Produces("application/json")]
  [Authorize(Roles = "Admin,Editor,Guest")]
  [ApiController]
  public class FeedbacksController : ControllerBase
  {
    private readonly Data.DbContext _context;
    private readonly IHttpContextAccessor _accessor;

    public FeedbacksController(Data.DbContext context, IHttpContextAccessor accessor)
    {
      _context = context;
      _accessor = accessor;
    }

    // GET: api/Feedbacks
    [HttpGet]
    [AllowAnonymous]
    [Route("api/Feedbacks")]
    public IActionResult GetFeedbacks(int pid) =>

    Ok(_context.Feedbacks
        .Where(x => x.ProductId == pid && (x.Approved || User.IsInRole("Admin")))
        .OrderByDescending(x => x.CreatedDate));

    // GET: api/Feedbacks/5
    [HttpGet]
    [Route("api/Feedbacks/{id}")]
    public async Task<IActionResult> GetFeedback([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id == -1)//isnew
      {
        return Ok(new Feedback());
      }


      var feedback = await _context.Feedbacks.SingleOrDefaultAsync(m => m.Id == id);

      if (feedback == null)
      {
        return NotFound();
      }

      return Ok(feedback);
    }



    // PUT: api/Feedbacks/5
    [HttpPut]
    [Route("api/Feedbacks/{id}")]
    public async Task<IActionResult> PutFeedback([FromRoute] int id, [FromBody] Feedback feedback)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != feedback.Id)
      {
        return BadRequest();
      }

      _context.Entry(feedback).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!FeedbackExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return Ok(feedback);
    }

    // POST: api/Feedbacks
    [HttpPost]
    [Route("api/Feedbacks")]
    public async Task<IActionResult> PostFeedback([FromBody] Feedback feedback)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      feedback.IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
      feedback.CreatedDate = DateTime.Now;
      _context.Feedbacks.Add(feedback);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GeFeedback", new { id = feedback.Id }, feedback);
    }

    // DELETE: api/Feedbacks/5
    [HttpDelete]
    [Route("api/Feedbacks/{id}")]
    public async Task<IActionResult> DeleteFeedback([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var feedback = await _context.Feedbacks.SingleOrDefaultAsync(m => m.Id == id);
      if (feedback == null)
      {
        return NotFound();
      }

      _context.Feedbacks.Remove(feedback);
      await _context.SaveChangesAsync();

      return Ok(feedback);
    }

    private bool FeedbackExists(int id)
    {
      return _context.Feedbacks.Any(e => e.Id == id);
    }
  }
}
