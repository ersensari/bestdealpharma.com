using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace bestdealpharma.com.Helpers
{
  public class TestEmailService : IEmailService
  {
    private readonly IConfiguration _configuration;

    public TestEmailService(IConfiguration configuration)
    {
      _configuration = configuration;
    }

    public async Task SendEmail(string email, string subject, string message)
    {
      using (var client = new SmtpClient())
      {
        var credential = new NetworkCredential
        {
          UserName = _configuration["TestEmail:UserName"],
          Password = _configuration["TestEmail:Password"]
        };

        client.Credentials = credential;
        client.Host = _configuration["TestEmail:Host"];
        client.Port = int.Parse(_configuration["TestEmail:Port"]);
        client.EnableSsl = true;
        client.Timeout = 10000;
        using (var emailMessage = new MailMessage())
        {
          emailMessage.To.Add(new MailAddress(email));
          emailMessage.From = new MailAddress(_configuration["TestEmail:Email"]);
          emailMessage.Subject = subject;
          emailMessage.Body = message;
          emailMessage.IsBodyHtml = true;
          try
          {
            client.Send(emailMessage);
          }
          catch
          {
            //Console.WriteLine(e);
            //throw e;
          }
        }
      }

      await Task.CompletedTask;
    }
  }
}
